﻿using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using Vidly.Models;
using Vidly.ViewModels;

namespace Vidly.Controllers
{
    public class CustomersController : Controller
    {

        public ActionResult New()
        {
            var viewModel = new NewCustomerViewModel
            {

            };
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult Create()
        {
            return View();
        }

        
        [ValidateAntiForgeryToken]

        public ViewResult Index()
        {
            return View();
        }


    }
}